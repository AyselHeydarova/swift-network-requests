//
//  TableViewCell.swift
//  Network(Branches)
//
//  Created by Aysel Heydarova on 10/9/20.
//

import UIKit

class TableViewCell: UITableViewCell {
    @IBOutlet weak var branchNameLabel: UILabel!
    @IBOutlet weak var branchAddressLabel: UILabel!
}


