//
//  ViewController.swift
//  Network(Branches)
//
//  Created by Aysel Heydarova on 10/9/20.
//

import UIKit

class ListViewController: BaseViewController {
    
    @IBOutlet weak var tableView: UITableView!
        
    override func viewDidLoad() {
        super.viewDidLoad()
        
        title = "ATMS"
        branchesVM.getBranches(language: currentLanguage) {
            print(self.currentLanguage)
            self.tableView.reloadData()
        }
    }
}

extension ListViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return branchesVM.branchesArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! TableViewCell
        cell.branchNameLabel.text = branchesVM.branchesArray[indexPath.row].name
        cell.branchAddressLabel.text = branchesVM.branchesArray[indexPath.row].address
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
}

extension ListViewController: ReloadTableViewDelegate {
    
    func didLanguageChange() {
        self.tableView.reloadData()
    }
}
