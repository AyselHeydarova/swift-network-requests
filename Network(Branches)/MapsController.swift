
import UIKit
import MapKit

class MapsController: BaseViewController {

    @IBOutlet weak var map: MKMapView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        branchesVM.getBranches(language: currentLanguage) {
            self.addMapPins()
        }
        title = "Map View"
        navigationController?.title = "Map View"
        
        let initialLocation = CLLocation(latitude: 40.409264, longitude: 49.867092)
        map.centerToLocation(initialLocation)
        
    }
    
    func addMapPins() {
        for branch in branchesVM.branchesArray {
            let annotation = MKPointAnnotation()
            annotation.title = branch.name
            annotation.coordinate = CLLocationCoordinate2D(latitude: branch.latitude ?? 0, longitude: branch.longitude ?? 0)
            map.addAnnotation(annotation)
        }
    }
}

private extension MKMapView {
  func centerToLocation(
    _ location: CLLocation,
    regionRadius: CLLocationDistance = 100000
  ) {
    let coordinateRegion = MKCoordinateRegion(
      center: location.coordinate,
      latitudinalMeters: regionRadius,
      longitudinalMeters: regionRadius)
    setRegion(coordinateRegion, animated: true)
  }
}


