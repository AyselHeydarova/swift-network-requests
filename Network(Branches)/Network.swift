import Foundation
import Alamofire

enum languages: String, CaseIterable {
    case AZ = "az"
    case EN = "en"
    case RU = "ru"
}

class NetworkAlamofire {
    
    static let urlAddress = "https://mbanking-prod.ibar.az/api/v2/locator-svc/locators/"
    
    static func executeRequest(language: String,  completion: @escaping ([Branch]) -> ()){
       let header: HTTPHeaders = [
            "Accept-Language": language
        ]
        let request = AF.request(urlAddress, method: .get, headers: header)
        request.responseDecodable(of: BranchData.self) { (response) in
            guard let branchData = response.value else { return }
            completion(branchData.data ?? [])
        }
    }
}










//class Network {
//
//    static let urlAddress = "https://mbanking-prod.ibar.az/api/v2/locator-svc/locators/"
//
//    static func performRequest(language: String, completion: @escaping ([Branch]) -> ()) {
//        let url = URL(string: urlAddress)
//        let session = URLSession(configuration: .default)
//        var request = URLRequest(url: url!)
//        request.httpMethod = "GET"
//        request.addValue(language, forHTTPHeaderField: "Accept-Language")
//        let task =  session.dataTask(with: request) { (data, response, error) in
//            if error != nil {
//                print("session error", error!)
//            }
//            if let safeData = data {
//                let decoder = JSONDecoder()
//                let branches = try? decoder.decode(BranchData.self, from: safeData)
//                completion(branches?.data ?? [])
//            }
//        }
//        task.resume()
//    }
//
//}


