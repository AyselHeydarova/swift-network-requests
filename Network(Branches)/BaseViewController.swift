
import UIKit

protocol ReloadTableViewDelegate {
    func didLanguageChange()
}

class BaseViewController: UIViewController {
    
    var branchesVM = BranchesViewModel()
    var currentLanguage = "az"
    var delegate: ReloadTableViewDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setNavigationButton(title: currentLanguage, titleColor: .white, isRightButton: true)
    }
    
    func setNavigationButton(title: String?, titleColor: UIColor?, isRightButton: Bool) {
        navigationItem.rightBarButtonItem = UIBarButtonItem(title: title, style: .plain, target: self, action: #selector(buttonTapped))
        navigationItem.rightBarButtonItem?.tintColor = titleColor
    }
    
    @objc func buttonTapped() {
        branchesVM.showLanguages(viewController: self)
    }
}

extension BaseViewController: LanguageControllerDelegate {
    func languageSelection(lang: languages) {
    navigationItem.rightBarButtonItem?.title = lang.rawValue
      currentLanguage = lang.rawValue
        branchesVM.getBranches(language: currentLanguage) {
            self.delegate?.didLanguageChange()
        }
    }
}
