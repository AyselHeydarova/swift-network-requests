//
//  LanguagesViewController.swift
//  Network(Branches)
//
//  Created by Aysel Heydarova on 10/13/20.
//

import UIKit

protocol LanguageControllerDelegate {
    func languageSelection(lang: languages)
}

struct Languages {
    var lang: String?
    var type: languages
}

class LanguagesViewController: UIViewController {

    @IBOutlet weak var langView: UIView!
    @IBOutlet weak var label: UILabel!
    @IBOutlet weak var closeButton: UIButton!
    @IBOutlet weak var tableView: UITableView!
    
    var delegate: LanguageControllerDelegate?
    var arrLangs = [Languages(lang: "Azerbaijan", type: .AZ), Languages(lang: "Russian", type: .RU), Languages(lang: "English", type: .EN)]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        label.text = "Languages"
    }
    
    @IBAction func closeButtonTapped(_ sender: UIButton) {
        dismiss(animated: true) {
        }
    }
}

extension LanguagesViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrLangs.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "languageCell",for: indexPath) as! LanguageTableViewCell
        cell.languageLabel.text = arrLangs[indexPath.row].lang
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        //protocol action
        delegate?.languageSelection(lang: arrLangs[indexPath.row].type)
        dismiss(animated: true, completion: nil)
    }
}
