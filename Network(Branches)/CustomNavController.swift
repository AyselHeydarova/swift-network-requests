//
//  CustomNavControllerViewController.swift
//  Network(Branches)
//
//  Created by Aysel Heydarova on 10/13/20.
//

import UIKit

class CustomNavController: UINavigationController {
    var navigationBarAppearance = UINavigationBar.appearance()
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationBarAppearance.barTintColor = .blue
        navigationBarAppearance.titleTextAttributes = [NSAttributedString.Key.foregroundColor:UIColor.white]
    }
}
