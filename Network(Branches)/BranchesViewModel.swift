
import Foundation
import UIKit

class BranchesViewModel {
    var branchesArray = [Branch]()
    
    func getBranches(language: String, complete: @escaping ()->()) {
        NetworkAlamofire.executeRequest(language: language) { (branchArr) in
            self.branchesArray = branchArr
            complete()
        }
    }
    
    func showLanguages(viewController: UIViewController) {
        let languageVC = viewController.storyboard?.instantiateViewController(identifier: "LanguagesViewController") as? LanguagesViewController
        languageVC?.delegate = viewController as? LanguageControllerDelegate
        languageVC?.modalPresentationStyle = .overCurrentContext
        viewController.present(languageVC!, animated: true, completion: nil)
    }
    
}

