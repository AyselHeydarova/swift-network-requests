//
//  Branch.swift
//  Network(Branches)
//
//  Created by Aysel Heydarova on 10/9/20.
//

import Foundation

struct BranchData: Codable {
    var data: [Branch]?
}
 struct Branch: Codable {
    let id: Int?
    let extId: String?
    let type: String?
    let latitude: Double?
    let longitude: Double?
    let workOnWeekend: Bool?
    let atmCashIn: Bool?
    let name: String?
    let address: String?
}
