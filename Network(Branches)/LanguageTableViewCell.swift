//
//  LanguageCellTableViewCell.swift
//  Network(Branches)
//
//  Created by Aysel Heydarova on 10/13/20.
//

import UIKit

class LanguageTableViewCell: UITableViewCell {

    @IBOutlet weak var languageLabel: UILabel!
    
}
